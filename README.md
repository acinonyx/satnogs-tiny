# SatNOGS Tiny: an embedded version of SatNOGS 
![SatNOGS](assets/SatNOGS-logo-vertical-black.png)

satnogs-tiny is an embedded version of [SatNOGS](https://satnogs.org/) that
aims to run on simple and cheap hardware.

## Hardware

The development is currently taking place on the
[TTGO-LoRa32-V2.1](https://github.com/LilyGO/TTGO-LoRa32-V2.1) board and it
is not known if it can work on a different board.

## Installation

The installation can be done using [PlatformIO](https://platformio.org/).
Platformio can be installed as a plugin for many IDEs. You can find a complete
list here: https://docs.platformio.org/en/latest/ide.html#desktop-ide

One of the simplest options is to use VSCode, you can find installation guides at
[VSCode - PlatformIO Documentation](https://docs.platformio.org/en/latest/ide/vscode.html)

## Development Guide

The development is performed on the `master` branch.
For special cases where a team of developers should work an a common feature,
maintainers may add a special branch on the repository.
However, it will be removed at the time it will be merged on the `master` branch.
All developers should derive the `master` branch for their feature branches and merge
requests should also issued at this branch.

Make sure also that you sign your work following the rules described in the
[CONTRIBUTING.md](CONTRIBUTING.md).

## Debugging

For easier debugging a separate JTAG capable debugger can be used. In particular, the use
of [ESP-Prog](https://github.com/espressif/esp-iot-solution/blob/master/docs/en/hw-reference/ESP-Prog_guide.rst)
has been tested. ESP-Prog is one of the development and debugging tools from Espressif.
Provides automatic firmware downloading, serial communication, and JTAG online debugging.

Instructions for using ESP-Prog can be found at the link below:
https://docs.platformio.org/en/latest/plus/debug-tools/esp-prog.html

The JTAG pin assignment can be found at the table below:

|   JTAG Pin   | Board Pin |
|    :----:    |   :----:  |
| TCK		   | IO13	   |
| TDI		   | IO12	   |
| TDO		   | IO15	   |
| TMS		   | IO14	   |
| GND		   | GND	   |

## License

![SatNOGS](assets/SatNOGS-logo-vertical-black.png)
![Libre Space Foundation](assets/LSF_HD_Horizontal_Color1-300x66.png) 
&copy; 2021 [Libre Space Foundation](https://libre.space).

Licensed under the [GPLv3](LICENSE).
